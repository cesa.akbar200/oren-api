<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tabel_Maintanance extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'tabel_maintanances';
    protected $fillable = [
        'id_ac',
        'last_cuci',
        'last_freon',
        'last_service'
    ];

    public function tabel_ac()
    {
        return $this->belongsTo(Tabel_Ac::class);
    }
}
