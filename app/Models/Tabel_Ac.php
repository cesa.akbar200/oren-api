<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tabel_Ac extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'tabel_acs';
    protected $fillable = [
        'id_property',
        'nama_ac',
        'pk_ac'

    ];

    public function tabel_maintanance()
    {
        return $this->hasMany(Tabel_Maintanance::class);
    }

    protected $hidden = [];
}
