<?php

namespace App\Http\Controllers;

use App\Helpers\ApiFormatter;
use App\Models\Tabel_Ac;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TabelAcController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Tabel_Ac::all();

        if($data){
            return ApiFormatter::createApi(200, 'Success', $data);
        }else{
            return ApiFormatter::createApi(400, 'Failed');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Tabel_Ac::create([
            'id_property' => $request->id_property,
            'nama_ac' => $request->nama_ac,
            'pk_ac' => $request->pk_ac
        ]);

        return response()->json([
            'data' => $data
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tabel_Ac  $tabel_Ac
     * @return \Illuminate\Http\Response
     */
    public function show(Tabel_Ac $tabel_Ac)
    {
        return response()->json([
            'data' => $tabel_Ac
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tabel_Ac  $tabel_Ac
     * @return \Illuminate\Http\Response
     */
    public function edit(Tabel_Ac $tabel_Ac)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tabel_Ac  $tabel_Ac
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tabel_Ac $tabel_Ac)
    {
        $tabel_Ac->id_property = $request->id_property;
        $tabel_Ac->nama_ac = $request->nama_ac;
        $tabel_Ac->pk_ac = $request->pk_ac;
        $tabel_Ac->save();

        return response()->json([
            'data' =>$tabel_Ac
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tabel_Ac  $tabel_Ac
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tabel_Ac $tabel_Ac)
    {
        $tabel_Ac->delete();
        return response()->json([
            'message' => 'id_property delete'
        ], 204);
    }
}
