<?php

namespace App\Http\Controllers;

use App\Models\Tabel_Ac;
use Illuminate\Http\Request;
use App\Helpers\ApiFormatter;
use App\Models\Tabel_Maintanance;
use App\Http\Controllers\Controller;

class TabelMaintananceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Tabel_Maintanance::all();

        if($data){
            return ApiFormatter::createApi(200, 'Success', $data);
        }else{
            return ApiFormatter::createApi(400, 'Failed');
        }


        Tabel_Maintanance::with('tabel_ac')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tabel_Maintanance  $tabel_Maintanance
     * @return \Illuminate\Http\Response
     */
    public function show(Tabel_Maintanance $tabel_Maintanance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tabel_Maintanance  $tabel_Maintanance
     * @return \Illuminate\Http\Response
     */
    public function edit(Tabel_Maintanance $tabel_Maintanance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tabel_Maintanance  $tabel_Maintanance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tabel_Maintanance $tabel_Maintanance)
    {

        $tabel_Maintanance::where('id_ac', $request->id_ac)
              ->update(['last_cuci' => $request->last_cuci,'last_freon'=>$request->last_freon,'last_service' => $request->last_service]);


        return response()->json([
            'data' => $tabel_Maintanance
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tabel_Maintanance  $tabel_Maintanance
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tabel_Maintanance $tabel_Maintanance)
    {
        //
    }
}
